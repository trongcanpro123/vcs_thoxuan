<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class DriverModel extends BaseModel
{
    protected $table = 'manager_car_in_out';
    protected $primaryKey = 'id';
    
    protected $useSoftDeletes = false;
    protected $allowedFields = ['checkin_image_up', 'checkin_image_front', 'checkin_image_left', 'checkin_image_right', 'checkout_image_up', 'checkout_image_front', 'checkout_image_left', 'checkout_image_right', 'construction_id', 'construction_name', 'construction_address', 'receipt', 'delivery_unit', 'car_id', 'car_type', 'material_type', 'material_unit', 'delivery_method', 'input_volume', 'reduction_volume', 'actual_volume', 'car_number', 'status', 'checkin_time', 'checkout_time', 'created_by', 'created_time', 'udpated_by', 'udpated_time', 'record_status'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function export_excel_filter($car_numer)
    {

        return $this->db->query('SELECT * FROM manager_car_in_out  WHERE pump_system_id = ? AND area_id = ? AND NOT id = ?', [$pump_system_id, $area_id, $this->getPrimaryKey()])->getResultArray();
    }
    public function select_export_excel()
    {
        return $this->db->query('SELECT  `car_number`, `car_type` FROM manager_car_in_out')->getResultArray();
    }
    public function getRules(string $scenario = null): array
    {
        return [
            'checkin_image_up' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh chính diện không được để trống',
                ]
            ],
            'checkin_image_front' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh trái không được để trống',
                ]
            ],
            'checkin_image_left' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh phải không được để trống',
                ]
            ],
            // 'construction_name' =>[
            //     'rules'  => 'required',
            //     'errors' => [
            //         'required' => 'Tên công trình không được để trống',
            //     ]
            // ],
            // 'car_number' =>[
            //     'rules'  => 'required',
            //     'errors' => [
            //         'required' => 'Biển số xe không được để trống',
            //     ]
            // ],
            // 'actual_volume' =>[
            //     'rules'  => 'required',
            //     'errors' => [
            //         'required' => 'Khối thực nhập phải lơn hơn 0',
            //     ]
            // ],

        ];
    }

    

}