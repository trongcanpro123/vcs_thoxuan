<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;
use App\Models\CameraConfigModel;

/**
 * Class ContentCategoryModel
 * @package App\Models
 * @property string in_out
 * @property string title
 * @property int area_id
 * @property int camera_id
 */
class AreaCameraConfigModel extends BaseModel
{
    protected $table = 'area_camera_config';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['area_id','camera_id','title','in_out'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $dateFormat = 'int';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    public function getRules(string $scenario = null): array
    {
        return [
            ''
        ];
    }

    public function get_link_play(){
        $model_cam = (new CameraConfigModel())->where('id',$this->camera_id)->first();
        if($model_cam){
            return $model_cam->link_play;
        }
        return '';
    }
}
