<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class PumpProductTypeModel extends BaseModel
{
    protected $table = 'pump_product_type';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['product_name','product_type_code'];

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    public function getRules(string $scenario = null): array
    {
        return [
            'product_name' =>[
                'rules'  => 'required|min_length[3]|max_length[200]',
                'errors' => [
                    'required' => 'Tên mặt hàng không được để trống',
                    'min_length'=> 'Tên mặt hàng có ít nhất 3 ký tự',
                    'max_length'=> 'Tên mặt hàng có nhiều nhất 200 ký tự'
                ]
            ],
            'product_type_code' =>[
                'rules'  => 'required|min_length[3]|max_length[200]|is_unique[pump_product_type.product_type_code]',
                'errors' => [
                    'required' => 'Mã mặt hàng không được để trống',
                    'min_length'=> 'Mã mặt hàng có ít nhất 3 ký tự',
                    'max_length'=> 'Mã mặt hàng có nhiều nhất 200 ký tự',
                    'is_unique'=> 'Mã mặt hàng không được trùng lặp'
                ]
            ]
        ];
    }
}
