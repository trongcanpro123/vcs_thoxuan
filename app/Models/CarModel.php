<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class CarModel extends BaseModel
{
    protected $table = 'car';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['car_number', 'note', 'delivery_unit', 'drive_name', 'car_type'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function select_car_number($car_number)
    {
        return $this->db->query('SELECT * FROM car WHERE car_number = ?', [$car_number])->getRow();
    }
    public function select_car()
    {
        return $this->db->query('SELECT * FROM car')->getResultArray();
    }
    public function getRules(string $scenario = null): array
    {
        return [
            'car_number' =>[
                'rules'  => 'required|min_length[3]|max_length[255]|is_unique[car.car_number]',
                'errors' => [
                    'required' => 'Biển số không được để trống',
                    'min_length'=> 'Biển số có ít nhất 3 ký tự',
                    'max_length'=> 'Biển số có nhiều nhất 200 ký tự',
                    'is_unique'=> 'Biển số không được trùng lặp'
                ]
            ],
            'delivery_unit' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Đơn vị không được để trống',
                ]
            ],
            'car_type' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Loại xe không được để trống',
                ]
            ],
        ];
    }
}