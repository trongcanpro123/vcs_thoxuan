<style>
.image-driver{
    height:350px;
    width: 300px
}
.mr-bt {
    margin-bottom:25px;
    text-align:center;
}
.image-title {

}
.div-image {
    margin-bottom:15px;
}
.btn-capture {
    min-width:100px;
    padding: 7px 24px;
}
</style>

<?php

use App\Helpers\Html;

/**
 * @var \App\Models\DriverModel $model
 */
?>

<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="margin-bottom: 0px">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-text">
                            <h6 class="card-title">Chọn camera</h6>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_in" onchange="change_play_cam(this, 'videoElement')">
                                        <?php if($camera_in) {
                                            foreach ($camera_in as $camera){
                                                ?>
                                                <option value="<?= $camera->get_link_play() ?>" ><?= $camera->title ?></option>

                                                <?php
                                            }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <video width="100%" id="videoElement"></video>
                        <canvas style="display: none" id="canvas_videoElement" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--    right-->
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Tên tài xế</label>
                            <?= Html::textInput('driver_name', $model->driver_name, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Số điện thoại di động</label>
                            <?= Html::textInput('phone_number', $model->phone_number, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'type' => 'number'
                            ]) ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Ảnh trái</h6>
                            </div>

                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="image_left" name="image_left" value="<?= $model->image_left ?>"/>
                                    <img id="img_snap_left" class="image_preview_snap driver_thum" src="<?= $model->image_left ? $model->image_left : '/images/empty.jpg' ?>" />

                                </div>
                                <button onclick="snap_video('videoElement','img_snap_left','image_left')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header card-header-info text-center">
                                <h6 class="card-title">Ảnh chính diện</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="image_front" name="image_front" value="<?= $model->image_front ?>"/>
                                    <img id="img_snap_front" class="image_preview_snap driver_thum"  src="<?= $model->image_front ? $model->image_front : '/images/empty.jpg' ?>" />
                                </div>
                                <button onclick="snap_video('videoElement','img_snap_front','image_front')" class="btn-capture btn btn-info btn-round"  type="button">Chụp</button>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Ảnh phải</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="image_right" name="image_right" value="<?= $model->image_right ?>"/>
                                    <img id="img_snap_right" class="image_preview_snap driver_thum"  src="<?= $model->image_right ? $model->image_right : '/images/empty.jpg' ?>" />
                                </div>
                                <button onclick="snap_video('videoElement','img_snap_right','image_right')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>


                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="<?= route_to('driver') ?>" class="btn btn-round">Huỷ</a>
                        <button style="margin-left:15px" class="btn btn-success btn-round" type="submit">Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


