<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Kiểm soát vào ra';
?>
<div class="row " id="div_check_in">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-text">
                    <h6 class="card-title">Kiểm tra xe vào</h6>
                </div>
            </div>
            <div class="card-body">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Công trình</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Phiếu nhập kho</label>
                                    <?= Html::textInput('note', '', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Địa điểm</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Biển số xe</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Đơn vị giao hàng</label>
                                    <?= Html::textInput('note', '', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Loại xe</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Loại vật liệu</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Phương pháp giao nhận</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Khối lượng nhập</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Khối lượng giảm trừ</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Khối lượng thực</label>
                                    <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                        'autocomplete' => 'off',
                                        'class' => 'form-control',
                                        'autofocus' => true,
                                        'style' => 'text-transform:uppercase'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-6 div_camera_in ">
                            <div class="card" style="margin-top: 20px">
                                <div class="card-body text-center">
                                    <video width="100%" id="videoElement_in"></video>
                                    <canvas style="display: none" id="canvas_videoElement_in" ></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 div_camera_out">
                            <div class="card" style="margin-top: 20px">
                                <div class="card-body text-center">
                                    <video width="100% text-center" id="videoElement_out"></video>
                                    <canvas style="display: none" id="canvas_videoElement_out" ></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Cam phía đầu xe</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Cam ảnh trên thùng</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-6 div_camera_in ">
                            <div class="card" style="margin-top: 20px">
                                <div class="card-body text-center">
                                    <video width="100%" id="videoElement_in"></video>
                                    <canvas style="display: none" id="canvas_videoElement_in" ></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 div_camera_out">
                            <div class="card" style="margin-top: 20px">
                                <div class="card-body text-center">
                                    <video width="100% text-center" id="videoElement_out"></video>
                                    <canvas style="display: none" id="canvas_videoElement_out" ></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cam ảnh thùng bên trái</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cam ảnh thùng bên phải</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="<?= route_to('bv_list_cars') ?>" class="btn btn-round">Huỷ</a>
                                <button style="margin-left:15px" class="btn btn-success btn-round"
                                        type="submit">Lưu
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
    <div class="col-md-1"></div>

</div>

<div id="qr_info_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Thông tin đơn hàng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-left "><h6>Mã mặt hàng</h6></th>
                        <th class="text-left"><h6>Tên mặt hàng</h6></th>
                        <th class="text-center"><h6>Số lượng</h6></th>
                    </tr>
                    </thead>
                    <tbody id="tbody_smo_order_detail">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

<div id="car_number_hand_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Xử lý tay biển số xe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" onchange="process_customer_type()" type="radio" name="customer_type" value="smo"
                                      <?= $model->customer_type == 'smo' ? 'checked': '' ?> >Khách SMO
                                <span class="circle">
                              <span class="check"></span>
                            </span>
                            </label>

                            <label class="form-check-label">
                                <input  class="form-check-input" onchange="process_customer_type()" type="radio" name="customer_type" value="not_smo"
                                    <?= $model->customer_type == 'not_smo' ? 'checked': '' ?>  > Khách Ngoài SMO
                                <span class="circle">
                              <span class="check"></span>
                            </span>
                            </label>



                        </div>
                    </div>
                    <div id="div_for_khach_le" class="col-sm-12 <?= $model->customer_type == 'not_smo' ? '': 'hidden' ?>  ">
                        <div class="form-group bmd-form-group">
                            <input id="form_car_number_hand" type="text" autocomplete="off" placeholder="Biển số xe" value="<?= $model->car_number_detected ?>"
                                   style="text-transform:uppercase" class="form-control">
                            <span class="bmd-help">Nhập biển số xe trong trường hợp khách lẻ không có QR code</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <input id="form_car_number_note" type="text" autocomplete="off" placeholder="Lý do/Ghi chú"
                                   required class="form-control">
                            <span class="bmd-help">Nhập lý do/ghi chú</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="update_car_number_note(<?= $model->id ?>)" class="btn btn-success">Lưu
                    lại
                </button>

            </div>
        </div>
    </div>
</div>

<div id="driver_hand_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Xử lý tay tài xế</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="div_for_khach_le" class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <input id="form_driver_hand" type="text" autocomplete="off" placeholder="Tên tài xế"
                                   class="form-control">
                            <span class="bmd-help">Nhập tài xế trong trường hợp khách lẻ</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <input id="form_driver_note" type="text" autocomplete="off" placeholder="Lý do/Ghi chú"
                                   required class="form-control">
                            <span class="bmd-help">Nhập lý do/ghi chú</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="update_driver_note(<?= $model->id ?>)" class="btn btn-success">Lưu lại
                </button>

            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        $('#qr_value').focus();
        $('#qr_value').select();
        var list_qr_code = <?= $list_qr_code ?>;
        localStorage.setItem("list_qr_code", JSON.stringify(list_qr_code));

        if(<?= $model->checkin_order > 0 ? '1':'0' ?>){
            $('#div_check_in').find('input, textarea, button, select').attr('disabled','disabled');
        }
    };
</script>