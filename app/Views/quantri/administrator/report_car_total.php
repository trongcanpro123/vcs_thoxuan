<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Báo cáo xe tổng hợp';
?>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="card-body ">
                <form action="<?php base_url() . '/Administrator/report_car_total'; ?>" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <select name="area_id" class="form-control mr-sm-2 " placeholder="Chọn kho">

                                <?php
                                $current_user = (\App\Models\AdministratorModel::findIdentity());
                                if($current_user->type_user == 'lanh_dao'){
                                    ?>
                                    <option value="0" >Tất cả</option>

                                    <?php
                                }
                                if ($area_models) {
                                    foreach ($area_models as $area) {
                                        if($current_user->type_user == 'lanh_dao' || $current_user->area_id == $area->id){
                                            ?>
                                            <option value="<?= $area->id ?>" <?php if ($area->id == $param_search['area_id']) {
                                                echo 'selected';
                                            } ?> ><?= $area->name ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>


                        </div>
                        <div class="col-md-3">
                            <div class='input-group date'>
                                <input type='text' name="start_date" id="from_date_in_car" placeholder="Từ ngày"
                                       class="form-control datepicker" autocomplete="off"
                                       value="<?= $param_search['start_date'] ?>"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class='input-group date'>
                                <input type='text' name="end_date" id="to_date_in_car" placeholder="Đến ngày"
                                       class="form-control datepicker" autocomplete="off"
                                       value="<?= $param_search['end_date'] ?>"/>
                            </div>
                        </div>
                        <div class="col-md-2 text-center">
                            <button class="btn btn-info btn-round" type="submit">Tìm kiếm</button>
                        </div>
                        <input type="hidden" name="tab" id ="tab_menu" value="<?= !empty($tab_menu) ? $tab_menu : '1'?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div style="background: #00aec5" class="card-header card-header-tabs card-header-rose">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                       <?php 
                         $tab_list_car = ($tab_menu == '1') ? 'show active' : '';
                         $tab_chart_report = ($tab_menu == '2') ? 'show active' : '';
                        ?>
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="nav-item">
                                <a class="nav-link tab-item-car <?= $tab_list_car ?>" data-tab="1" href="#profile" data-toggle="tab">
                                    <i class="material-icons">assignment</i>Số liệu xe tổng hợp
                                    <div class="ripple-container"></div>
                                    <div class="ripple-container"></div></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link tab-item-car <?= $tab_chart_report ?>" data-tab="2" href="#messages" data-toggle="tab">
                                    <i class="material-icons">timeline</i> Biểu đồ xe tổng hợp
                                    <div class="ripple-container"></div>
                                    <div class="ripple-container"></div></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane <?= $tab_list_car ?>" id="profile">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày</th>
                                <th>SL xe vào</th>
                                <th>SL xe ra</th>
                                <th>SL xe không hợp lệ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$report_result || empty($report_result) || !$report_result['so_lieu']): ?>
                                <tr>
                                    <td colspan="100">
                                        <div class="empty-block">
                                            <img src="/images/no-content.jpg" alt="No content"/>
                                            <h4>Không có nội dung</h4>
                                        </div>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <?php foreach ($report_result['so_lieu'] as $n => $model): ?>
                                    <tr>
                                        <td><?= $model['stt'] ?></td>
                                        <td><?= $model['ngay'] ?></td>
                                        <td><?= $model['slxv']  ?></td>
                                        <td><?= $model['slxr'] ?></td>
                                        <td><?= $model['slxkhl'] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane <?= $tab_chart_report ?>" id="messages">
                        <div class="card-body" >
                            <canvas id="report_xe_tong_hop" height="200px"
                                    style="display: block; box-sizing: border-box; height: 200px; "></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.onload = function () {
        var ctx = document.getElementById("report_xe_tong_hop").getContext('2d');
        const data = <?=  $report_result['bieu_do'] ?>;
        const config = {
            type: 'line',
            data: data,
            options: {
                responsive: true, maintainAspectRatio: true,
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true,
                            // stepSize: 500,
                        }
                    }],
                },
            }
        };
        var chart_bc_xe_tong_hop = new Chart(ctx, config);

        jQuery('#from_date_in_car').datetimepicker({
            format: 'd-m-Y',
            timepicker:false,
        });

        jQuery('#to_date_in_car').datetimepicker({
            format: 'd-m-Y',
            timepicker:false,
        });

        $('.tab-item-car').on('click', function(e){
            let tab_val = $(this).data('tab');
            $('#tab_menu').val(tab_val);
        })
    };
</script>